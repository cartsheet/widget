import React from 'react'
import { storiesOf } from '@storybook/react'
import PaymentForm from '../src/components/SidebarWidget/Checkout/PaymentForm'
import { Elements, StripeProvider } from 'react-stripe-elements';
import { Card } from 'antd'
import styled from 'styled-components'

const CardWrapper = styled.div`
  margin: 2rem;
`

storiesOf(`Payment`, module)
  .add(`default`, () => (
    <CardWrapper>
      <Card>
        <StripeProvider apiKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
          <Elements>
            <PaymentForm />
          </Elements>
        </StripeProvider>
      </Card>
    </CardWrapper>
  ))

import React, { Fragment } from 'react'
import SidebarWidget from './SidebarWidget/SidebarWidget'
import ProductsWidget from './ProductsWidget/ProductsWidget'

const App = () => {

  return (
    <Fragment>
      <SidebarWidget />
      <ProductsWidget />
    </Fragment>
  )
}

export default App
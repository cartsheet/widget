import { connect } from 'react-redux'
import { updateItemQuantity } from '../../../store/actions'
import Product from './Product'

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems
  }
}

const mapDispatchToProps = (dispatch, { id }) => {
  return {
    updateItemQuantity: (quantity) => {
      dispatch(updateItemQuantity(id, quantity))
    }
  }
}

const ProductContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Product)

export default ProductContainer
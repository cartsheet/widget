import { ADD_ITEM, REMOVE_ITEM, UPDATE_ITEM_QUANTITY, CLEAR_ITEMS, IS_LOADING, UPDATE_DATA } from './actionTypes'
import axios from 'axios'
import testProducts from './test-data/test-products'

const baseUrl = `https://testing.com/`

export const addItem = (item) => ({
  type: ADD_ITEM,
  item
})

export const removeItem = (itemId) => ({
  type: REMOVE_ITEM,
  itemId
})

export const clearItems = () => ({
  type: CLEAR_ITEMS
})

export const updateItemQuantity = (itemId, quantity) => ({
  type: UPDATE_ITEM_QUANTITY,
  itemId,
  quantity
})

export const isLoading = (isLoading) => ({
  type: IS_LOADING,
  isLoading
})

export const updateData = (dataType, data) => ({
  type: UPDATE_DATA,
  dataType,
  data
})

export const fetchProducts = (url, dataType) => {

  return async (dispatch) => {

    try {

      dispatch(isLoading(true))

      const data = await axios.get(`${baseUrl}${url}`)

      dispatch(updateData(dataType, data))

      dispatch(isLoading(false))
    } catch (err) {

      console.log(`error: `, err)

      dispatch(updateData(dataType, testProducts))

      dispatch(isLoading(false))
    }
  }
}
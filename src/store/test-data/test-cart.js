import shortid from 'shortid'

const cart = [
  {
    id: shortid.generate(),
    title: `Sock Package`,
    price: 1499,
    quantity: 2,
    previewImage: `https://images-na.ssl-images-amazon.com/images/I/91YxaJ2dc1L._UX679_.jpg`
  }
]

export default cart
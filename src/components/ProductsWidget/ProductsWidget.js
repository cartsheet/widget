import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import ProductsListContainer from './ProductsListContainer'

class ProductsWidget extends Component {

  constructor(props) {
    super(props)
    this.rootElement = document.getElementById(props.productListElement)
    this.el = document.createElement('div')
  }

  componentDidMount() {
    this.rootElement.appendChild(this.el)
  }

  componentWillUnmount() {
    this.rootElement.removeChild(this.el)
  }

  render() {
    return ReactDOM.createPortal(
      <ProductsListContainer />,
      this.el,
    );
  }
}

ProductsWidget.propTypes = {
  productListElement: PropTypes.string.isRequired
}

ProductsWidget.defaultProps = {
  productListElement: `cartsheet-product-list`
}

export default ProductsWidget
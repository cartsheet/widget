import React from 'react'
import { storiesOf } from '@storybook/react'
import '../src/styles/styles.scss'
import { Provider } from 'react-redux'
import ProductsWidget from '../src/components/ProductsWidget/ProductsWidget';
import SidebarWidget from '../src/components/SidebarWidget/SidebarWidget';
import store from '../src/store'

// Add DOM element
const productListElement = document.createElement('div');
productListElement.setAttribute('id', 'cartsheet-product-list');
document.body.append(productListElement);

const props = {
  applicationId: `dogs12345665433dogs`
}

const stories = storiesOf(`Full Document`, module)

stories
  .add(`Products & Sidebar`, () => (
    <Provider store={store}>
      <ProductsWidget {...props}/>
      <SidebarWidget {...props}/>
    </Provider>
  ))


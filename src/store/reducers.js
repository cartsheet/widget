import { ADD_ITEM, REMOVE_ITEM, UPDATE_ITEM_QUANTITY, CLEAR_ITEMS, IS_LOADING, UPDATE_DATA } from './actionTypes'
import shortid from 'shortid'
import produce from 'immer'
import isEmpty from 'lodash.isempty'
import testCart from './test-data/test-cart'

const initialState = {
  applicationId: `dogs-testing-12345678`,
  isLoading: false,
  cartItems: testCart,
  productItems: null
}

const reducer = produce((draft, action) => {
    switch (action.type) {
      case ADD_ITEM:

        draft.cartItems = produce(draft.cartItems, cartItemsDraft => {
          cartItemsDraft.push(Object.assign({}, action.item, {
            id: shortid.generate()
          }))
        })

        return
      case REMOVE_ITEM:

        draft.cartItems = produce(draft.cartItems, cartItemsDraft => {
          delete cartItemsDraft[cartItemsDraft.findIndex(item => item.id === action.itemId)]
        }).filter(item => !isEmpty(item))

        return
      case UPDATE_ITEM_QUANTITY:

        draft.cartItems = produce(draft.cartItems, cartItemsDraft => {
          cartItemsDraft[cartItemsDraft.findIndex(item => item.id === action.itemId)].quantity = action.quantity
        })

        return
      case CLEAR_ITEMS:

          draft.cartItems = []

          return
      case IS_LOADING:

          draft.isLoading = action.isLoading

          return
      case UPDATE_DATA:

          draft[action.dataType] = action.data

          return
      default:
        return draft
    }
  }, initialState)

export default reducer
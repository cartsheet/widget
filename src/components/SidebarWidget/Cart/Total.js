import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { formatPrice } from '../../../helpers'
import { Button } from 'antd'

const Wrapper = styled.div`
  width:100%;
  display: flex;
  align-items: center;
  justify-content: space-around;
  margin-top: auto;
  background-color: #fafafa;
  padding: 10px 0;
`

const Header = styled.div`
  font-size: .7rem;
  color: #888;
`

const Price = styled.div`
  font-size: 1.2rem;
`

const Total = ({total, navigate}) => {

  return (
    <Wrapper>
      <div>
        <Header>Total</Header>
        <Price>{formatPrice(total)}</Price>
      </div>
      <Button
        onClick={navigate}
        type="primary">Checkout</Button>
    </Wrapper>
  )
}

Total.propTypes = {
  total: PropTypes.number.isRequired
}

export default Total
import React from 'react'
import PropTypes from 'prop-types'
import Header from '../Sidebar/Header'
import PaymentForm from './PaymentForm'
import styled from 'styled-components'
import { Elements, StripeProvider } from 'react-stripe-elements';

const Wrapper = styled.div`
  width:100%;
  height:100%
  display: flex;
  flex-direction:column;
`

const Cart = ({ navigate }) => {

  return (
    <Wrapper>
      <Header
        title='Checkout'
        leftIcon='left'
        handleLeftClick={() => navigate(`cart`)}
      />
      <StripeProvider apiKey="pk_test_TYooMQauvdEDq54NiTphI7jx">
          <Elements>
            <PaymentForm />
          </Elements>
        </StripeProvider>
    </Wrapper>
  )
}

Cart.propTypes = {
}

export default Cart
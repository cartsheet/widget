import React, { useEffect } from 'react'
import ProductListItem from './ListItem/Item'
import isEmpty from 'lodash.isempty'
import styled from 'styled-components'

const ProductListWrapper =  styled.div`
  display:grid;
  grid-template-columns: repeat(3, 1fr);
  grid-gap: 10px;
  margin: 2rem;
`

const ProductsList = ({ productItems, isLoading, fetchProducts }) => {

  useEffect( () => {

    fetchProducts()
  }, [])

  if (isLoading) return <div>....loading</div>

  if (isEmpty(productItems)) return <p>is empty</p>

  const productList = productItems.map(item => <ProductListItem key={item.id} {...item} />)

  return (
    <ProductListWrapper>
      {productList}
    </ProductListWrapper>
  )
}

export default ProductsList
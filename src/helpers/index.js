import currency from 'currency.js'

export const formatPrice = price => currency(price / 100).format(true)
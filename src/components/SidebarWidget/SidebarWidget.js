import React, { useState } from 'react'
import TargetButton from './Target/Button'
import Sidebar from './Sidebar/Sidebar'
import Cart from './Cart/CartContainer'
import Checkout from './Checkout/Checkout'
import Slider from './Slider/Slider'

const SidebarWidget = () => {

  const [isOpen, setIsOpen] = useState(false);

  const TargetButtonIcon = isOpen ? `logout` : `shopping-cart`

  return (
    <Slider isOpen={isOpen}>
      <TargetButton
        icon={TargetButtonIcon}
        handleClick={() => setIsOpen(!isOpen)} />
      <Sidebar initialRoute='cart'>
        <Cart key='cart' />
        <Checkout key='checkout' />
      </Sidebar>
    </Slider>
  )
}

export default SidebarWidget
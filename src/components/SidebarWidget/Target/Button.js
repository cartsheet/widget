import React from 'react'
import { Icon } from 'antd'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const TargetButtonWrapper = styled.div`
  border-radius: 100%;
  padding: 15px;
  border: 15px;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
  transition: all 0.3s cubic-bezier(.25,.8,.25,1);
  cursor: pointer;
  font-size: 1.5rem;

  position: absolute;
  left: -70px;
  bottom: 14px;
  background-color: #fff;

  :hover {
    box-shadow: 0 8px 16px rgba(0,0,0,0.2), 0 4px 4px rgba(0,0,0,0.22);
  }
`

const InlineIconStyles = {
  display: `flex`,
  position: `relative`,
  left: -1
}

const TargetButton = ({ handleClick, icon }) => {

    return (
      <TargetButtonWrapper onClick={handleClick}>
        <Icon style={InlineIconStyles} type={icon} />
      </TargetButtonWrapper>
    )
}

TargetButton.propTypes = {
  handleClick: PropTypes.func.isRequired
}

export default TargetButton
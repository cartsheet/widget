import { connect } from 'react-redux'
import ProductsList from './ProductsList'
import { fetchProducts } from '../../store/actions';

const mapStateToProps = (state) => {
  return {
    isLoading: state.isLoading,
    productItems: state.productItems
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    fetchProducts: () => {
      dispatch(fetchProducts(`/products`, `productItems`))
    }
  }
}

const ProductsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductsList)

export default ProductsListContainer
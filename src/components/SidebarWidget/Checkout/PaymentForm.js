import React, {Component} from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { Input, Form, Button, Icon } from 'antd'
import { Formik } from 'formik';
import * as Yup from 'yup';
import styled from 'styled-components'

const { Item } = Form

const ResetButtonWrapper = styled.div`
  display:inline-flex;
  margin-top: 10px;
  margin-right: 10px;
`

const CardElementWrapper = styled.div`
  border: 1px solid #d9d9d9;
  border-radius: 4px;
  padding: 6px 11px;
  transition: border-color .2s

  :hover {
    border-color: #40a9ff;
  }
`

class CheckoutForm extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  async submit(e) {

    e.preventDefault()

    console.log(`submit`)
    // User clicked submit
  }

  render() {


    return (
      <Formik
        initialValues={{ email: '' }}
        onSubmit={(values, { setSubmitting }) => {
          setTimeout(() => {
            alert(JSON.stringify(values, null, 2));
            setSubmitting(false);
          }, 500);
        }}
        validationSchema={Yup.object().shape({
          email: Yup.string().email().required('Required'),
        })}
      >
        { props => {
          const { values, touched, errors, dirty, isSubmitting, handleChange, handleBlur, handleSubmit, handleReset } = props;
          return (
            <form onSubmit={handleSubmit}>
              <Item
                hasFeedback
                validateStatus={errors.name && touched.name ? `error` : ``}
                help={errors.name}
                style={{ marginBottom: 5 }}
              >
                <Input
                  name="name"
                  prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Name"
                  value={values.name}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Item>
              <Item
                hasFeedback
                validateStatus={errors.email && touched.email ? `error` : ``}
                help={errors.email}
                style={{ marginBottom: 5 }}
              >
                <Input
                  name="email"
                  prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                  placeholder="Email"
                  value={values.email}
                  onChange={handleChange}
                  onBlur={handleBlur}
                />
              </Item>
              <CardElementWrapper>
                <CardElement style={{
                  base: {
                    color: 'rgba(0, 0, 0, 0.65)',
                    fontWeight: 400,
                    fontFamily: `-apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Hiragino Sans GB', 'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'`,
                    fontSize: '14px',
                    '::placeholder': {
                      color: '#d9d9d9',
                    },
                  }
                }}/>
              </CardElementWrapper>
              <ResetButtonWrapper>
                <Button
                  type="default"
                  onClick={handleReset}
                  disabled={!dirty || isSubmitting}
                >
                  Reset
                </Button>
              </ResetButtonWrapper>
              <Button
                type="primary"
                disabled={isSubmitting}
                htmlType="submit"
              >
                Purchase
              </Button>
            </form>
          );
        }}
      </Formik>
    )
  }
}

export default injectStripe(CheckoutForm);
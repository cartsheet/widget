import React, { useState } from 'react'
import { storiesOf } from '@storybook/react'
import { withKnobs, boolean } from '@storybook/addon-knobs';
import '../src/styles/styles.scss'
import Sidebar from '../src/components/SidebarWidget/Sidebar/Sidebar'
import Cart from '../src/components/SidebarWidget/Cart/Cart'
import Checkout from '../src/components/SidebarWidget/Checkout/Checkout'
import Slider from '../src/components/SidebarWidget/Slider/Slider'
import TargetButton from '../src/components/SidebarWidget/Target/Button'
import styled from 'styled-components'

const MainWrapper = styled.div`
  width: 100vw;
  height: 100%;
  border: 2px solid black;
  overflow-x:hidden;
  position:relative;
`

const BodyWrapper = styled.div`
  background: linear-gradient(to bottom, #3b9b41 0%,#2bc7d9 31%,#145882 66%,#507f74 100%);
  width: 100%;
  height: 200vh
`

const ButtonStory = () => {

  const [isOpen, setIsOpen] = useState(true);

  return (
    <MainWrapper>
      <Slider isOpen={isOpen}>
        <TargetButton handleClick= {() => setIsOpen(!isOpen)} />
        <Sidebar>sidebar content</Sidebar>
      </Slider>
      <BodyWrapper />
    </MainWrapper>
  )
}

const stories = storiesOf(`Sidebar`, module)

stories.addDecorator(withKnobs);

stories
  .add(`default`, () => (<Sidebar />))
  .add(`slider`, () => {

    const isOpen = boolean(`isOpen`, true)

    return (
      <MainWrapper>
        <Slider isOpen={isOpen}><Sidebar /></Slider>
        <BodyWrapper />
      </MainWrapper>
    )
  })
  .add(`with button`, () => <ButtonStory />)
  .add(`cart`, () => {

    const isOpen = boolean(`isOpen`, true)

    return (
      <MainWrapper>
        <Slider isOpen={isOpen}>
          <Sidebar>
            <Cart />
          </Sidebar>
        </Slider>
        <BodyWrapper />
      </MainWrapper>
    )
  })
  .add(`navigation`, () => {

    const isOpen = boolean(`isOpen`, true)

    return (
      <MainWrapper>
        <Slider isOpen={isOpen}>
          <Sidebar initialRoute='cart'>
            <Cart key='cart' />
            <Checkout key='checkout' />
          </Sidebar>
        </Slider>
        <BodyWrapper />
      </MainWrapper>
    )
  })


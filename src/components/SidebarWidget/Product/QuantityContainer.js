import { connect } from 'react-redux'
import { updateItemQuantity, removeItem } from '../../../store/actions'
import Quantity from './Quantity'

const mapStateToProps = (state) => {
  return {}
}

const mapDispatchToProps = (dispatch, { id }) => {
  return {
    updateItemQuantity: (quantity) => {
      dispatch(updateItemQuantity(id, quantity))
    },
    removeItem: () => {
      dispatch(removeItem(id))
    }
  }
}

const QuantityContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Quantity)

export default QuantityContainer
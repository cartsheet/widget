import React from 'react'
import { storiesOf } from '@storybook/react'
import '../src/styles/styles.scss'
import { Provider } from 'react-redux'
import SidebarWidget from '../src/components/SidebarWidget/SidebarWidget';
import store from '../src/store'

const stories = storiesOf(`Sidebar Widget`, module)

const props = {
  applicationId: `dogs12345665433dogs`
}

stories
  .add(`default`, () => (
    <Provider store={store}>
      <SidebarWidget {...props}/>
    </Provider>
  ))


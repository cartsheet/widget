import { connect } from 'react-redux'
import Cart from './Cart'
import { clearItems } from '../../../store/actions';

const mapStateToProps = (state) => {
  return {
    cartItems: state.cartItems
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    clearItems: () => {
      dispatch(clearItems())
    }
  }
}

const CartContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)

export default CartContainer
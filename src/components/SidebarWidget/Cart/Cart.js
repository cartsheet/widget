import React from 'react'
import PropTypes from 'prop-types'
import Header from '../Sidebar/Header'
import Total from './Total'
import EmptyCart from './EmptyCart'
import Product from '../Product/ProductContainer'
import styled from 'styled-components'
import isEmpty from 'lodash.isempty'

const Wrapper = styled.div`
  width:100%;
  height:100%
  display: flex;
  flex-direction:column;
`

const Products = styled.div``

const Cart = ({ navigate, cartItems, updateItemQuantity, clearItems }) => {

  if (isEmpty(cartItems)) return <EmptyCart/>

  const productList = cartItems.map(product => <Product key={product.id} updateItemQuantity={updateItemQuantity} {...product} />)
  const total = cartItems.reduce((a, b) => ((a.quantity * a.price) + (b.quantity * b.price)), { quantity: 0, price: 0 })

  return (
    <Wrapper>
      <Header
        title='Cart'
        rightIcon='delete'
        rightTooltip='Clear Cart'
        handleRightClick={clearItems}
      />
      <Products>{productList}</Products>
      <Total total={total} navigate={() => navigate(`checkout`)} />
    </Wrapper>
  )
}

Cart.propTypes = {
  cartItems: PropTypes.array,
  navigate: PropTypes.func
}

export default Cart
import React from 'react'
import { storiesOf } from '@storybook/react'
import TargetButton from '../src/components/SidebarWidget/Target/Button'

storiesOf(`Target`, module)
  .add(`button`, () => <TargetButton handleClick= {() => console.log(`handleclick`)} />)

import React from 'react'
import { formatPrice } from '../../../helpers'
import styled from 'styled-components'
import Quantity from './QuantityContainer'

const ProductWrapper = styled.div`
  display:flex;
  justify-content:space-between;
  border-bottom:1px solid #ececec;
  padding: 0 10px;
`

const ImageWrapper = styled.div`
  width:70px;
  height:70px;
  padding: 10px;
`

const Image = styled.img`
  width:100%;
  height:100%;
  object-fit: contain;
`

const InformationWrapper = styled.div`
  flex: 1;
  margin: 0 10px;
  display:flex;
  flex-direction:column;
  align-items:flex-start;
  justify-content:center;
`

const Title = styled.p`
  margin:0;
`

const Price = styled.p`
  margin:0;
  font-size:.7rem;
`

const Product = ({ id, title, price, quantity, previewImage }) => {

  return (
    <ProductWrapper>
      <ImageWrapper>
        <Image  src={previewImage}/>
      </ImageWrapper>
      <InformationWrapper>
        <Title>{title}</Title>
        <Price>{formatPrice(price)}</Price>
      </InformationWrapper>
      <Quantity id={id} quantity={quantity} />
    </ProductWrapper>
  )
}

export default Product
import React from 'react'
import Header from '../Sidebar/Header'
import styled from 'styled-components'
import { Empty } from 'antd'

const Wrapper = styled.div`
  width:100%;
  height:100%
  display: flex;
  flex-direction:column;
`
const EmptyWrapper = styled.div`
  width: 100%;
  height: 100%;
  display:flex;
  align-items: center;
  justify-content: center;
`

const EmptyCart = () => (
  <Wrapper>
    <Header title='Cart' />
    <EmptyWrapper>
      <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No items in your cart" />
    </EmptyWrapper>
  </Wrapper>
)

export default EmptyCart
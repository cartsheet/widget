import React from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import './Slider.scss'

const Slider = ({ isOpen, children }) => {

  const transitionClass = classNames({
    'slider': true,
    'translate-none': true,
    'translate-right-full': !isOpen
  })

  return (
    <div className={transitionClass}>
      {children}
    </div>
  )
}

Slider.propTypes = {
  isOpen: PropTypes.bool.isRequired
}

Slider.defaultProps = {
  isOpen: true
}

export default Slider
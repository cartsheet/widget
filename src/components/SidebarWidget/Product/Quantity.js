import React from 'react'
import { Icon } from 'antd'
import styled from 'styled-components'

const QuantityWrapper = styled.div`
  display:flex;
  flex-direction:column;
  align-items: center;
  justify-content: center;
`

const QuantityInteger = styled.span`
  font-size: .8rem;
`

const Quantity = ({ quantity, updateItemQuantity, removeItem }) => {

  const downIconType = quantity === 1 ? `delete` : `caret-down`
  const onHandleDownClick = quantity === 1 ? removeItem : updateItemQuantity

  return (
    <QuantityWrapper>
      <Icon
        onClick={() => updateItemQuantity(quantity + 1)}
        type="caret-up" />
      <QuantityInteger>{quantity}</QuantityInteger>
      <Icon
        onClick={() => onHandleDownClick(quantity - 1)}
        type={downIconType} />
    </QuantityWrapper>
  )
}

export default Quantity
import React, { useState } from 'react'
import styled from 'styled-components'
import isNil from 'lodash.isnil'

const Sidebar = ({ initialRoute, children }) => {

  const SidebarWrapper = styled.div`
    width: 300px;
    height: 100%;
  `

  const defaultRoute = isNil(initialRoute) ? children[0].key : initialRoute

  const [ navigationRoute, setNavigationRoute ] = useState(defaultRoute)

  const additionalProps = {
    navigationRoute,
    navigate: route => setNavigationRoute(route)
  }

  const selectedRouteComponent = children.find(child => child.key === navigationRoute)

  const childWithProps = React.cloneElement(selectedRouteComponent, additionalProps)

  return (
    <SidebarWrapper>
      {childWithProps}
    </SidebarWrapper>
  )
}

export default Sidebar
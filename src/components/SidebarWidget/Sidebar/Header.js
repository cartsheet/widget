import React from 'react'
import { Icon, Tooltip } from 'antd'
import styled from 'styled-components'

const Header = ({ title = `Cart`, leftIcon = ``, rightIcon = ``, leftTooltip=``, rightTooltip = ``, handleLeftClick, handleRightClick }) => {

  const HeaderWrapper = styled.header`
    width: 100%;
    padding: 5px 10px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    background-color: #fafafa;
  `

  const Title = styled.h3`
    margin: 0;
    font-weight: 600;
  `

  return (
    <HeaderWrapper>
      <Tooltip placement="bottomLeft" title={leftTooltip}>
        <Icon
          type={leftIcon}
          onClick={handleLeftClick}
        />
      </Tooltip>
      <Title>{title}</Title>
      <Tooltip placement="bottomRight" title={rightTooltip}>
        <Icon
          type={rightIcon}
          onClick={handleRightClick}
        />
      </Tooltip>
    </HeaderWrapper>
  )
}

Header.propTypes = {
}

export default Header